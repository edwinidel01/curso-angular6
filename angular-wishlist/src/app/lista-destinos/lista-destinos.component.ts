import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DestinoViaje } from './../models/destino-viaje.model'

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.scss']
})

export class ListaDestinosComponent implements OnInit {
	destinos: DestinoViaje[];
  constructor() { 
  	this.destinos = [];
  }

  ngOnInit(): void {
  }

  guardar(nombre:string, imagepath:string):boolean {
    this.destinos.push(new DestinoViaje(nombre, imagepath))
    return false;
  }

}
